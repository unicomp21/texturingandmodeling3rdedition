/* Copyright 1994, 2002 by Steven Worley
   This software may be modified and redistributed without restriction
   provided this comment header remains intact in the source code.
   This code is provided with no warrantee, express or implied, for
   any purpose.
   
   A detailed description and application examples can be found in the
   1996 SIGGRAPH paper "A Cellular Texture Basis Function" and
   especially in the 2002 book "Texturing and Modeling, a Procedural
   Approach, 3rd edition." There is also extra information on the web
   site http://www.worley.com/cellular.html .

   If you do find interesting uses for this tool, and especially if
   you enhance it, please drop me an email at steve@worley.com. */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "poisson.h" /* Prototype */


/* A utility function which computes samples to fill the Poisson_count
   array. This is used only if you want to manipulate the density of
   points in space. This is often done to find the most EFFICIENT
   density to compute the cellular values with. If the density is too
   high, then time will be wasted looping through too many sample
   points per cube, most of which are thrown out. If density is too low,
   then too many cubes will have to be tested to guarantee that we've
   found the closest points. Therefore we adjust the mean Poisson
   density simply to make the computation speed as fast as possible,
   then use DENSITY_ADJUSTMENT to renormalze the scale of the
   function. This routine is included in case you want to tune to your
   own environment, or modify the function in other ways and want to
   reoptimize the algorthm's speed.
   This function will always clip a cell's discrete density to be at least 1.
   This is to prevent empty cells since we want to limit our cube search.
   
   Poisson densities are computed using the recurrence:
   P(0, u)=exp(-u)
   P(n, u)=P(n-1,u)*u/n

   But because of floating point roundoff, this calculation
   becomes more shaky for large u, I wouldn't trust it much for u>100.
   For u<200, it seems that accuracy is better than 0.00001 for all n.
   
   Strategy. Form an accumulation of the discrete Poisson probabilties
   and find which ones the range 0..1 maps into. */


#define MAX_POISSON_COUNT 128  /* we don't need more entries than this. It's
				  good enough for density up to 64 or so. */

void MakePoissonLookup(double density, double zero_boost,
		       long lookup[POISSON_ARRAY_LENGTH])
{
  double accum[MAX_POISSON_COUNT];
  long i, swap, index;
  unsigned long seed;
  
  /* Fill with the discrete Poisson distribution probabilities. Uses
     recurrence relation. Roundoff dangerous for large u (u>200 or so) */
  accum[0]=exp(-density); 
  for (i=1; i<MAX_POISSON_COUNT; i++) accum[i]=accum[i-1]*density/i;
  
  /* sum individual probabilities to make a cumulative distribution function */
  for (i=1; i<MAX_POISSON_COUNT; i++) accum[i]+=accum[i-1];
  
  /* Scale up the 0..1 range to match the length of the sample array */
  for (i=0; i<MAX_POISSON_COUNT; i++) accum[i]*=(POISSON_ARRAY_LENGTH+1.0);
  
  /* set the last cumulative value to huge, so we never run off the end.. */
  accum[MAX_POISSON_COUNT-1]=POISSON_ARRAY_LENGTH+99999.0;
  
  /* We now have the ranges of index numbers to fill stored as floats,
     now fill the array based on those values */  
  index=0;  
  for (i=0; i<POISSON_ARRAY_LENGTH; i++)
    {
      /* advance our density until we're at the cutoff. We use a
	 bias of 1.0 to avoid always adding a 0 sample at the start. */
      while (accum[index]<i+1.0) index++;  
      lookup[i]=index;
    }

  /* Optional. SCRAMBLE the order of the array to help make it more
     randomized. Uses not-great but adequate LCG random number generator. */
  seed=12345;
  for (i=0; i<POISSON_ARRAY_LENGTH; i++)
    {
      seed=723748125*seed+2488143859;
      index=(seed>>12)%POISSON_ARRAY_LENGTH; /* not great but doesn't use LSB */
      swap=lookup[i];
      lookup[i]=lookup[index];
      lookup[index]=swap;
    }

  /* OPTIONAL final step. Remove this for true Poisson tables!

     If we have too many 0 and 1 values, we may have too low of a
     local density to insure that the central 27 cubes are sufficient,
     which would be Bad. We can solve this by increasing the Poisson
     density, which is what should be done for best quality.  But we
     can make a faster-computing solution by relaxing the Poisson
     requirement and forcing some of the 0 and 1 sample cubes to have
     1 extra sample.  This is faster than having an "honest" higher
     Poisson density. It also tends to make the feature points more
     evenly distributed but it DOES reduce the isotropy of the texture
     which is a shame. However, visually, the results look fine, so I
     use a mostly-isotropic density that's tweaked for speed.
     
     Actually we have a variable called zero_boost. This is the percentage
     of 0s we changed.   The 1's are less likely to be boosted.. after some
     empirical testing, I boost zero_boost^2 percent of them.  
     
     And, finally, we clip too HIGH densities to be limited too, since
     these waste time in a different way, by spending too much time
     evaluating the list of points. Again, this clipping introduces
     bias, but much less than the 0 boosting.  */

  for (i=0; i<POISSON_ARRAY_LENGTH; i++)
    {
      long max_pop;

      max_pop=(long)ceil(2.0*density);
      if (max_pop<3) max_pop=3; /* for very low densities */

      seed=1402024253*seed+586950981; /* churn the seed */

      if (lookup[i]==0 && 
	  (seed+0.5)*(1.0/4294967296.0)<zero_boost) lookup[i]++;

      else if (lookup[i]==1 && 
	       (seed+0.5)*(1.0/4294967296.0)<zero_boost*zero_boost) lookup[i]++;

      /* clip high densities. This does not affect too many values though. */
      if (lookup[i]>max_pop) lookup[i]=max_pop;
    }

  return;
}
