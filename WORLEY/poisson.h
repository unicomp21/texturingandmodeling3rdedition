/* Copyright 1994, 2002 by Steven Worley
   This software may be modified and redistributed without restriction
   provided this comment header remains intact in the source code.
   This code is provided with no warrantee, express or implied, for
   any purpose.
   
   A detailed description and application examples can be found in the
   1996 SIGGRAPH paper "A Cellular Texture Basis Function" and
   especially in the 2002 book "Texturing and Modeling, a Procedural
   Approach, 3rd edition." There is also extra information on the web
   site http://www.worley.com/cellular.html .

   If you do find interesting uses for this tool, and especially if
   you enhance it, please drop me an email at steve@worley.com. */


#define POISSON_ARRAY_LENGTH 256

/* Generate a randomized table of populations that follow a Poisson
   distribution. <zero_boost> will artifically inflate population 0 and 
   1 values to try to avoid them  [0.0 means no values are inflated,
   1.0 means all of them are.] 
   
   This function is designed to be used to make lookup tables for
   cellular texturing. If you want it for other purposes, you probably
   want to edit the source code to take the final part of the algorithm
   which boosts low populations and clips high ones. */

void MakePoissonLookup(double density, double zero_boost,
		       long lookup[POISSON_ARRAY_LENGTH]);

