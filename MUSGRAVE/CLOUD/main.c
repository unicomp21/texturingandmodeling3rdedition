#include <stdio.h>
#include <math.h>
#include "cloud.h"

/*
 * The QAEB procedural ray tracing routine.
 *
 * Brute-force method steps across terrain at pixel-sized steps
 * until an intersection is found.  
 *
 * Copyright 1998 F. Kenton Musgrave
 * All rights reserved
 */

/* declare the global variables */
Light		light;		/* the single (prototype) light source */
Color		background;	/* the background color */
CameraType	camera;		/* the viewing specifications structure */
OptionsType	options;	/* storage for various options */
unsigned long	samples;	/* number of texture samples taken */
unsigned long	hits;		/* number of texture samples "inside" cloud */
Color		*scanline;	/* a scanline of image data */
double		*distances;	/* a scanline-row of distance values */
double		octaves_scalar;	/* magic number relating distance to octaves */
FILE		*outfile;	/* the image file */

main( int argc, char *argv[] )
{
	Init( argc, argv, &options, &camera, &light, &background, &octaves_scalar );
	Render( scanline, outfile );
	Cleanup( scanline, outfile );

	exit(0);

} /* main() */

