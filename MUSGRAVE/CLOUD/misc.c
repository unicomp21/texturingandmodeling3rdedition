#include <stdio.h>
#include <math.h>
#include "cloud.h"

/*
 * The QAEB procedural ray tracing routine.
 *
 * Miscellaneous functions module.
 *
 * Copyright 1998 F. Kenton Musgrave
 * All rights reserved
 */


/*
 * Returns the spatial position of "ray" at distance "t".
 *
 * Probably should be made an in-line function for efficiency.
 */
Vector
Ray_Pos( Ray *ray, double t )
{
	Vector	position;

	position.x = ray->origin.x + t * ray->dir.x;
	position.y = ray->origin.y + t * ray->dir.y;
	position.z = ray->origin.z + t * ray->dir.z;

	return position;

} /* Ray_Pos() */


/*
 * Normalize the vector argument; return 1/(vector length).
 */
double
Normalize(Vector *vec)
{
        double scale_factor;

	/* get inverse of length, to save on floating point divisions */
        scale_factor = 1.0/sqrt(vec->x*vec->x + vec->y*vec->y + vec->z*vec->z);

        vec->x *= scale_factor;
        vec->y *= scale_factor;
        vec->z *= scale_factor;

        return(scale_factor);

} /* Normalize() */

