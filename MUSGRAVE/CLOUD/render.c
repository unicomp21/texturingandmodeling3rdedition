#include <stdio.h>
#include <math.h>
#include <sys/types.h>
#include <malloc.h>
#include "cloud.h"

/*
 * The QAEB procedural ray tracing routine.
 *
 * Ray tracing module: samples the virtual screen.
 *
 * Copyright 1998 F. Kenton Musgrave
 * All rights reserved
 */

extern CameraType	camera;
extern OptionsType	options;
extern unsigned long    samples;
extern unsigned long    hits;

/*
 * A very primitive ray tracing routine:
 * samples a virtual screen at one ray/pixel;
 * currently checks only for intersection with diplacement map.
 */
Render( Color *scanline, FILE *outfile )
{
	int		row, column;
	double		increment, density, stride;
	Ray		ray, Gen_Ray();
	HitData		hit;
	Color		GetLight(), Integrate_Cloud();
	ColorChar	*outline, CharRGB();

		/* allocate memory for output scanline */
	outline = (ColorChar *)calloc( (size_t)camera.hres,
				       (size_t)sizeof( ColorChar ) );

		/* calculate the all-important ray-creep increment */
	increment = options.step_scale * camera.pix_width;

	for ( row=camera.vres-1; row>=0; row-- ) {
		for ( column=0; column<camera.hres; column++ ) {
			ray = Gen_Ray( row, column );
			scanline[column] = Integrate_Cloud( increment, &ray );
				/* gamma correct and quantize */
        		outline[column] = CharRGB( scanline[column] );
		}
		fwrite( (void *)outline, (size_t)sizeof( ColorChar ), 
			(size_t)camera.hres, outfile ); 
		fflush( outfile );
		if ( !(row % 50) )
			fprintf( stderr, "Finished scanline %d\n", row );
	}

		/* output some stats */
	fprintf( stderr, "samples taken:\t%ld\n", samples );
	fprintf( stderr, "samples in cloud:\t%ld\n", hits );
	fprintf( stderr, "average density (cloud-to-void ratio): %g\n",
			  (double) hits/samples );

} /* Render() */


/*
 * Generate a generic eye-ray.
 */
Ray
Gen_Ray( int row, int column )
{
	Ray	ray;

	ray.origin = camera.eyep;

	ray.dir.x= camera.corner_ray.x +
		   column * camera.pix_width * camera.right_dir.x +
		   row * camera.pix_height * camera.up_dir.x;
	ray.dir.y= camera.corner_ray.y +
		   column * camera.pix_width * camera.right_dir.y +
		   row * camera.pix_height * camera.up_dir.y;
	ray.dir.z= camera.corner_ray.z +
		   column * camera.pix_width * camera.right_dir.z +
		   row * camera.pix_height * camera.up_dir.z;

	NORM( (&ray.dir) );

	return( ray );

} /* Gen_Ray() */

