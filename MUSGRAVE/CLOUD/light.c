#include <stdio.h>
#include <math.h>
#include "cloud.h"

/*
 * The QAEB procedural ray tracing routine.
 *
 * Illumination calculation module.
 *
 * Copyright 1998 F. Kenton Musgrave
 * All rights reserved
 */

extern Light		light;
extern CameraType	camera;
extern OptionsType	options;

/*
 * The illumination model -- this routine needs to be filled out!
 *
 * (Currently it's a crude implementation of Beer's Law.)
 */
Color
GetLight( Ray *ray, HitData *hit, double density )
{
	double	intensity, exp();
	Color	sample;

	/* insert shading model here */
	/* Beer's Law, for now */
	intensity = 1.0 - exp(-density);

	sample.r = intensity * light.color.r;
	sample.g = intensity * light.color.g;
	sample.b = intensity * light.color.b;

	return( sample );

} /* GetLight() */


/* 
 * Change 0..1 RGB values to 0..255 RGB unsigned char values.
 */
ColorChar
CharRGB ( Color color )
{
	ColorChar	col;
	static int	first=TRUE;
	static double	gamma_val;
	double		drand48();

	if ( first ) {
		first = FALSE;
		gamma_val = 1.0/options.gamma;
	}

		/* render-time gamma correcting */
	if ( options.gamma != 1.0 ) {
		color.r = pow( color.r, gamma_val );
		color.g = pow( color.g, gamma_val );
		color.b = pow( color.b, gamma_val );
	}

		/* for dithering of quantization when post-processing */
	if ( options.noise ) {
		color.r += (drand48() - 0.5) * options.noise;
		color.g += (drand48() - 0.5) * options.noise;
		color.b += (drand48() - 0.5) * options.noise;
	}

		/* clamp and convert color values */
	if (color.r > 0.998)	col.rc = 255;
	else if (color.r < 0.)	col.rc = 0;
	else			col.rc = color.r*255. + 0.5;

	if (color.g > 0.998)	col.gc = 255;
	else if (color.g < 0.)	col.gc = 0;
	else 			col.gc = color.g*255. + 0.5;

	if (color.b > 0.998)	col.bc = 255;
	else if (color.b < 0.)	col.bc = 0;
	else			col.bc = color.b*255. + 0.5;

	return (col);

} /* CharRGB() */

