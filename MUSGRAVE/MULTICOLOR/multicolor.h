/* 
 * multicolor.h
 *
 * Header file for "multicolor" test program
 *
 * Copyright 6/12/98 F. Kenton Musgrave
 * All rights reserved
 */

#define RES	512
	/* this must be 3 times the above value */
#define LENGTH	1536

#define VADD(u, v)	( (u).x+=(v).x, (u).y+=(v).y, (u).z+=(v).z, (u) )
#define SMULT(s, v)	( (v).x*=(s), (v).y*=(s), (v).z*=(s), (v) )

typedef double Matrix[4][4];

typedef struct {
	double x;
	double y;
	double z;
} Vector;


typedef struct {
        double r;
        double g;
        double b;
} Color;


/* function prototypes */
void Multicolor(Vector texture,
		Color *color,
		double arg0, double arg1, double arg2, double arg3, 
		double arg4, double arg5, double arg6, double arg7, 
		double arg8, double arg9, double arg10 );
Vector Wrinkled( Vector point, double lacunarity, double H, double octaves );
Vector VecNoise( Vector point );
double Noise3( Vector point );
Vector VecNoise3( Vector point );
void init( void );
void CopyMatrix( Matrix a, Matrix b );
void MultMatrix( Matrix a, Matrix b, Matrix c );
void RotateX( double theta, Matrix m );
void RotateY( double theta, Matrix m );
void RotateZ( double theta, Matrix m );
Vector VectTransform( Vector v, Matrix m );
double Multifractal( Vector pos, double H, double lacunarity, double octaves, 
		     double zero_offset );

