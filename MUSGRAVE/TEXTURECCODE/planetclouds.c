/*
 * C code used to produce images appearing in:
 *
 * "Texturing and Modeling: A Procedural Approach," by David S. Ebert, ed.,
 * F. Kenton Musgrave, Darwyn Peachey, Ken Perlin, and Steven Worley.
 * Academic Press, 1998.  ISBN 0-12-228730-4.
 *
 * Note that this is the original C texture code, on which the Larry Gritz's
 * RenderMan shaders that appear in the text were based.
 *
 * Copyright 1998 F. Kenton Musgrave
 * All rights reserved.
 */

/* 
 * Texture to simulate clouds on a planetary scale; designed to be
 * mapped onto a transparent sphere around an Earth-like planetoid.
 *
 * Called as:
 * texture pclouds 3.0 0.7 2.0 7 3 0.2
 */
double PlanetClouds(
		Vector texture, 
		double p0, double p1, double p2, double p3, 
		double p4, double p5, double p6, double p7 )
{
	Vector	p, s;
	double	result;

	/* get distortion vector */
	if ( p6 )
		p = VecfBm( texture, p6, 1.9, p7 );
	else	p = VecNoise( texture );

	/* scale "weirdness" */
	SMULT( p0, p );
	/* insert "weirdness" */
	s = texture;
	SMULT( p4, s );
	VADD( p, s );
	/* "VLfBm()" is fBm constructed from "VLNoise()" */
	result = VLfBm( p, p1, p2, p3 );
	/* move the zero crossing */
	result += p5;
	if ( result < 0. ) result = 0.;
	result /= 1. + p5;

	return( result );

}  /* PlanetClouds() */

