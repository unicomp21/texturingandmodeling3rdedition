/*
 * A bonus texture, described in, but not appearing in, the book:
 *
 * "Texturing and Modeling: A Procedural Approach," by David S. Ebert, ed.,
 * F. Kenton Musgrave, Darwyn Peachey, Ken Perlin, and Steven Worley.
 * Academic Press, 1998.  ISBN 0-12-228730-4.
 *
 * Copyright 1998 F. Kenton Musgrave
 * All rights reserved.
 */


/*
 * The unpretentiously-named "generalized impressionistic texture."
 *
 * The argument "matrix" is the GIT autocorrelation matrix.
 *
 * Called as:
 * texture  git  purple2.mat  0.7 8 .4 .5
 *
 * Example matrix:
 * 0.608783  -0.656772  -0.445011  0.000000  
 * -0.147325  -0.067156  -0.102430  0.000000  
 * 0.134961  0.461755  -0.496854  0.000000  
 * -0.352941  -0.427451  -0.223529  1.000000  
 */
void GIT(	Vector texture,
		Colour *color,
		Matrix matrix,
		double H, double octaves, double scale )
{
	Vector	cvec;

		/* this could be any vector-valued function... */
	cvec = VecfBm(texture, 2.0, H, octaves );

		/* scale the effect */
	SCALE( scale, cvec );

		/* multiply the vector by the matrix */
	cvec = VectTransform(cvec, matrix);

	color->red =	cvec.x;
	color->green =	cvec.y;
	color->blue =	cvec.z;

	/* "reflect" values out of range */
	if ( color->red < 0.0 )		color->red = -color->red;
	if ( color->green < 0.0 )	color->green = -color->green;
	if ( color->blue < 0.0 )	color->blue = -color->blue;
	if ( color->red > 1.0 )		color->red -= color->red - 1.0;
	if ( color->green > 1.0 )	color->green -= color->green - 1.0;
	if ( color->blue > 1.0 )	color->blue -= color->blue - 1.0;

	/* if still out of range, clamp to range [0,1] */
	if ( color->red < 0.0 )		color->red = 0.0;
	if ( color->green < 0.0 )	color->green = 0.0;
	if ( color->blue < 0.0 )	color->blue = 0.0;
	if ( color->red > 1.0 )		color->red = 1.0;
	if ( color->green > 1.0 )	color->green = 1.0;
	if ( color->blue > 1.0 )	color->blue = 1.0;

} /* GIT() */


/* 
 * Below is the texture used in "Zabriskie Point," pre GIT.
 *
 * Note my comment presaging the GIT scheme.
 */

/* 
 * A "Painted Desert" texture.
 * The idea is to provide a nice 1/f bump vector plus a vector for
 * randomly perturbing the color of the surface.
 * Note that the major axes of the latter should be be able to be
 * oriented arbitrarily in RGB space, but are currently constrained
 * to the R, G, & B axes. 
 */
void DesertTexture( 
		Vector *bump, Vector *colorPert, Vector texture, 
		double textScale, double colorScale, double alpha, 
		double octaves, double which_noise, double dist )
{
	register Vector	point;
	static double	H;
	static int		first = TRUE;

	/* get the scaled bump map */
	point.x = texture.x * textScale;
	point.y = texture.y * textScale;
	point.z = texture.z * textScale;
	if( first )	{	/* initialize spectral exponent H for fBm */
		H = pow(2., (-0.5-alpha) );
		first = FALSE;
	}

	*bump= VecfBm( point, H, 2.0, 4.0 );

	/* do the scaled color perturbation vector */
	texture.x *= colorScale;
	texture.y *= colorScale;
	texture.z *= colorScale;

	/* choose the type of noise we want */
	if( which_noise == 1 ) { 	
		/* "VLfBm()" is fBm built from VLNoise() */
		colorPert->x= VLfBm( texture, H, 2.0, 4.0 );
		texture.x += 5.0;
		colorPert->y= VLfBm( texture, H, 2.0, 4.0 );
		texture.y += 5.0;
		colorPert->z= VLfBm( texture, H, 2.0, 4.0 );
	} else if ( which_noise == 2 ) {
		octaves -= log(dist);
		if ( octaves > 15.0 ) octaves = 15.0;
		*colorPert = VecfBm( texture, 2.0, 0.833, octaves );
	} else {
		/* "Wrinkled()" is Ken Perlin's vector-valued 1/f noise */
		*colorPert = Wrinkled( texture, 2.0, 0.833, octaves );
	}

	return;

} /* DesertTexture() */

