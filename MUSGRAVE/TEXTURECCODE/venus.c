/*
 * C code used to produce images appearing in:
 *
 * "Texturing and Modeling: A Procedural Approach," by David S. Ebert, ed.,
 * F. Kenton Musgrave, Darwyn Peachey, Ken Perlin, and Steven Worley.
 * Academic Press, 1998.  ISBN 0-12-228730-4.
 *
 * Note that this is the original C texture code, on which the Larry Gritz's
 * RenderMan shaders that appear in the text were based.
 *
 * Copyright 1998 F. Kenton Musgrave
 * All rights reserved.
 */

/* 
 * Planetray clouds with Coriolis effect, 
 * i.e., twist of cloud texture as the square of the radius.
 *
 * Called as:
 * texture venus 0.5 0.22 1 0.65 8 0
 */
void Venus( Vector texture, Vector intersect,
		Colour *colour,
		double scale, double twist, double offset, 
		double omega, double octaves, double coord_switch )
{
	double	radius_sq, angle, sine, cosine, value;
	Vector	point;

	if ( coord_switch )
		radius_sq = texture.x*texture.x + texture.y*texture.y;
	else
		radius_sq = intersect.x*intersect.x + intersect.y*intersect.y;
	angle = twist*TWOPI*radius_sq;
	sine = sin( angle );
	cosine = cos( angle );
	point.x = texture.x*cosine - texture.y*sine;
	point.y = texture.x*sine + texture.y*cosine;
	point.z = texture.z;

	/* "VLfBm()" is fBm built from "VLNoise()" */
	value = offset + scale*VLfBm( point, omega, 2.0, octaves );
	if ( coord_switch )
		if ( value < 0. ) value = 0.0;
	else
		if ( value < 0. ) value = -value;
	colour->red *= value;
	colour->green *= value;
	colour->blue *= value;

} /* Venus() */

