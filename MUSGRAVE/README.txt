README

This is the code accompanying Ken "Doc Mojo" Musgrave's chapters in
"Texturing and Modeling: A Procedural Approach," by David S. Ebert, ed.,
F. Kenton Musgrave, Darwyn Peachey, Ken Perlin, and Steven Worley,
Morgan Kaufmann, 2002. ISBN 1-55860-848-6. 

There are four directories or folders:

Cloud
GritzShaders
Multicolor
TextureCCode

Cloud:

This contains a complete, minimal C implementation of a QAEB ray tracer.
It is capable of rendering both terrains and clouds.  Compiling it as is, it
will render an image of a cloud with single scattering.

The basic fractal and multifractal functions described in Chapters 14 and
16 reside in fractal.c in this directory.

GritzShaders:

This contains all of Larry Gritz's RenderMan shaders.

Multicolor:

This contains a micro-renderer that makes an image of the "multicolor"
texture described in Chapter 11.  This texture is so brittle, that I include
the entire renderer, just to show that it *can* work, though it'd be hard
to reproduce accurately, as in a RenderMan shader.

TextureCCode:

This is the original C code on which Larry's RenderMan shaders are based.
The two generally give significantly different "looks." 
