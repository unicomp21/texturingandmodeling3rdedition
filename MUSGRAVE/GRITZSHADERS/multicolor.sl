/*
 * multicolor.sl - a multifractal color experiment
 *
 * DESCRIPTION:
 *    Creates a wildly colorful texture, intended to look like a
 * 	painting.
 *
 *
 * HINTS:
 *    See the C implementation (in ../Multicolor) for the original code.
 *
 *
 * AUTHOR: Ken Musgrave
 *    Conversion to Shading Language and other minor changes by Larry 
Gritz.
 *
 * REFERENCES:
 *    _Texturing and Modeling: A Procedural Approach_, by David S. 
Ebert, ed.,
 *    F. Kenton Musgrave, Darwyn Peachey, Ken Perlin, and Steven Worley.
 *    Academic Press, 1998.  ISBN 0-12-228730-4.
 *
 * HISTORY:
 *    1996 - original texture developed by Ken Musgrave.
 *    Apr 1998 - Conversion to Shading Language by L. Gritz
 *
 * last modified 20 Apr 1998 by lg
 */



#include "noises.h"


/*
 * Evaluates function at "pos", returns value stored in "y".
 *
 * Aperiodic version w/ noise basis function.
 */
vector VecMultifract(point p; float H, lacunarity, octaves, zero_offset)
{
    point pos = p;
    float f = 1, i;
    vector y = 1;

    for (i = 0;  i < octaves;  i += 1) {
	y *= zero_offset + f * (2*vector noise(pos) - 1);
	f *= H;
	pos *= lacunarity;
    }
    return y;
} /* VecMultifract() */



/*
 * a multifractal multicolor texture experiment
 */
surface multicolor (float arg0 = 0.7;
		    float arg1 = 2.0;
		    float arg2 = 8.0;
		    float arg3 = 0.2;
		    float arg4 = 0.5;
		    float arg5 = 8.0;
		    float arg6 = 4.0;
		    float arg7 = 5.0;
		    float arg8 = 1.0;
		    float arg9 = 1.0;
		    float arg10 = 4.0e5;
    )
{
    vector	cvec, axis;
    point tx = transform ("shader", P);
    float i;

    axis  = arg6 * vfBm (tx, filterwidthp(tx), arg5, 2.0, arg4);
    cvec = tx * 0.3;
    cvec  = .5 * arg7 * vfBm (cvec, filterwidthp(tx), 7, 2.0, 0.5);
    tx += cvec;
    
    cvec += arg10 * VecMultifract(tx, arg0, arg1, arg2, arg3);

    cvec = rotate (cvec, 3*length(axis), point(0,0,0), axis);

    Ci = .045 * arg8 * color (xcomp(cvec), ycomp(cvec), zcomp(cvec));
    Ci += Cs;
    for (i = 0;  i < 3;  i += 1) {
	float c = abs (comp(Ci,i));
	if (c > 1)
	    setcomp (Ci, i, 1-c);
    }
    Oi = Os;
    Ci *= Oi;
} /* multicolor() */
