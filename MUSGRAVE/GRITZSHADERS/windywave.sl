/*
 * windywave.sl -- displacement shader for water waves modulated by a
 *                 wind field.
 *
 * DESCRIPTION:
 *    Two octaves of noise make waves appropriate for a lake or other large
 *    body of water.  This displacement is modulated by another turbulent
 *    term which accounts for wind variations across the lake.
 *
 * PARAMETERS:
 *    Km - overall amplitude scale for the waves
 *    txtscale - overall frequency scaling for the waves
 *    windfreq - lowest frequency of the wind variations
 *    windamp - amplitude of the wind variation
 *    minwind - minimum wind value
 *
 * AUTHOR:
 *    C language version by F. Kenton Musgrave
 *    Translation to Shading Language by Larry Gritz.
 *
 * REFERENCES:
 *    _Texturing and Modeling: A Procedural Approach_, by David S. Ebert, ed.,
 *    F. Kenton Musgrave, Darwyn Peachey, Ken Perlin, and Steven Worley.
 *   Academic Press, 1998.  ISBN 0-12-228730-4.
 *
 * HISTORY:
 *    ??? - original C language version by Ken Musgrave
 *    Apr 94 - translation to Shading Language by L. Gritz
 *    Apr 1998 - modern SL and antialiasing by lg
 *
 * last modified 4 Apr 1998 by lg
 */


#include "noises.h"


displacement
windywave (float Km = 0.1;
	   float txtscale = 1;
	   float windfreq = 0.5;
	   float windamp = 1;
	   float minwind = 0.3)
{
    float offset;
    point PP;
    float wind;
    float turb;
    float filtwidth;

    PP = txtscale * windfreq * transform ("shader", P);
    filtwidth = filterwidthp (PP);

    offset = Km * fBm (PP,filtwidth,2,2,0.5);
    PP *= 8;  filtwidth *= 8;
    turb = turbulence (PP, filtwidth, 4, 2, 0.5);
    wind = minwind + windamp * turb;

    N = calculatenormal (P+wind * offset * normalize(N));
}
