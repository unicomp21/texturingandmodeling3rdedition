/*
 * puffyclouds.sl -- RenderMan compatible surface shader for puffy
 *                   clouds.
 *
 * DESCRIPTION:
 *    Makes nice looking cumulous clouds like you would see in the sky
 *    on a bright sunny day.  Works as a basic thresholded fBm.  Since
 *    this texture is generally used as a backdrop, it does not take
 *    lighting into account.  If you wanted a lit surface that looked like
 *    puffy clouds (like painted clouds on a wall), then it would be pretty
 *    easy to add the lighting.
 *
 * PARAMETERS:
 *    txtscale - overall scaling factor
 *    skycolor, cloudcolor - the obvious meanings
 *    octaves, omega, lambda - control the fractal appearance of the clouds
 *    threshold - fBm sum below this level is just blue sky
 *
 * AUTHOR:
 *    C language version by F. Kenton Musgrave
 *    Translation to RenderMan Shading Language by Larry Gritz.
 *
 * REFERENCES:
 *    _Texturing and Modeling: A Procedural Approach_, by David S. Ebert, ed.,
 *    F. Kenton Musgrave, Darwyn Peachey, Ken Perlin, and Steven Worley.
 *   Academic Press, 1998.  ISBN 0-12-228730-4.
 *
 * HISTORY:
 *    ??? - original C language version by Ken Musgrave
 *    Apr 94 - translation to Shading Language by L. Gritz
 *    Apr 1998 - modern SL and antialiasing by lg
 *
 * last modified 4 Apr 1998 by lg
 */


#include "noises.h"


surface
puffyclouds (float Ka = 0, Kd = 0;
	     float txtscale = 1;
	     color skycolor = color(.15, .15, .6);
	     color cloudcolor = color(1,1,1);
	     float octaves = 8, omega = 0.5, lambda = 2;
	     float threshold = 0;
	     )
{
    float value;
    color Ct;      /* Color of the surface */
    point PP;      /* Surface point in shader space */

    PP = txtscale * transform ("shader", P);

    /* Use fractional Brownian motion to compute a value for this point */
    value = fBm (PP, filterwidthp(PP), octaves, lambda, omega);

    Ct = mix (skycolor, cloudcolor, smoothstep (threshold, 1, value));
  
    /* Shade like matte, but use color Ct */
    Ci = Ct;   /* This makes the color disregard the lighting */
    /* Uncomment the next line if you want the surface to actually be lit */
    /*  Ci = Ct * (Ka * ambient() + Kd * diffuse(faceforward(N,I))); */
}
