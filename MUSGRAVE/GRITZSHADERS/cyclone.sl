/*
 * cyclone.sl - surface for a semi-opaque cloud layer to be put on an
 *              earth-like planetary model to model clouds and a cyclone.
 *
 * DESCRIPTION:
 *      When put on a sphere, sets the color & opacity of the sphere to
 *   make it look like the clouds surrounding an Earth-like planet, with
 *   a big cyclone.
 *      The shader works by creating a fractal turbulence function over
 *   the surface, then modulating the opacity based on this function in
 *   a way that looks like clouds on a planetary scale.
 *
 *
 * PARAMETERS:
 *    Ka, Kd - the usual meaning
 *    cloudcolor - the color of the clouds, usually white
 *    max_radius
 *    twist - controls the twisting of the clouds due to the cyclone.
 *    offset, scale - control the linear scaling of the cloud value.
 *    omega, octaves - controls the fractal characteristics of the clouds
 *
 *
 * HINTS:
 *    See the "planetclouds" shader for hints which apply equally well
 *    to this shader.
 *
 *
 * AUTHOR: Ken Musgrave
 *    Conversion to Shading Language and other minor changes by Larry Gritz.
 *
 * REFERENCES:
 *    _Texturing and Modeling: A Procedural Approach_, by David S. Ebert, ed.,
 *    F. Kenton Musgrave, Darwyn Peachey, Ken Perlin, and Steven Worley.
 *   Academic Press, 1998.  ISBN 0-12-228730-4.
 *
 * HISTORY:
 *    ???? - original texture developed by Ken Musgrave.
 *    Feb 1994 - Conversion to Shading Language by L. Gritz
 *    Apr 1998 - modern SL and antialiasing by lg
 *
 * last modified 4 Apr 1998 by lg
 */



#define TWOPI (2*PI)

#include "noises.h"




surface
cyclone (float Ka = 0.5, Kd = 0.75;
	 float max_radius = 1;
	 float twist = 0.5;
	 float scale = .7, offset = .5;
	 float omega = 0.675;
	 float octaves = 4;)
{
    float radius, dist, angle, eye_weight, value;
    point Pt;                 /* Point in texture space */
    vector PN;                /* Normalized vector in texture space */
    point PP;                 /* Point after distortion */
    float filtwidth, a;

    /* Transform to texture coordinates */
    Pt = transform ("shader", P);
    filtwidth = filterwidthp (Pt);

    /* Rotate hit point to "cyclone space" */
    PN = normalize (vector Pt);
    radius = sqrt (xcomp(PN)*xcomp(PN) + ycomp(PN)*ycomp(PN));

    if (radius < max_radius) {   /* inside of cyclone */
	/* invert distance from center */
	dist = pow (max_radius - radius, 3);
	angle = PI + twist * TWOPI * (max_radius-dist) / max_radius;
	PP = rotate (Pt, angle, point(0,0,0), point(0,0,1));
	/* Subtract out "eye" of storm */
	if (radius < 0.05*max_radius) {  /* if in "eye" */
	    eye_weight = (.1*max_radius - radius) * 10;   /* normalize */
	    /* invert and make nonlinear */
	    eye_weight = pow (1 - eye_weight, 4);
	}
	else eye_weight = 1;
    }
    else {
	PP = Pt;
	eye_weight = 0;
    }

    if (eye_weight > 0) {   /* if in "storm" area */
	/* Compute VLfBm */
	a = VLfBm (PP, filtwidth, octaves, 2, omega, 1);
	value = abs (eye_weight * (offset + scale * a));
    }
    else value = 0;

    /* Thin the density of the clouds */
    Oi = value * Os;

    /* Shade like matte, but with color scaled by cloud opacity */
    Ci = Oi * (Ka * ambient() + Kd * diffuse(faceforward(normalize(N),I)));
}
