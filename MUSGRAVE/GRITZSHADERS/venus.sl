/*
 * venus.sl - surface for a very cloudy planet like Venus.
 *
 *
 * DESCRIPTION:
 *      When put on a sphere, sets the color to look like a densely
 *   clouded planet, very much like the real Venus appears in UV.
 *      The shader works by creating a fractal turbulence function over
 *   the surface to simulate the clouds.  Strong Coriolis forces are
 *   simulated to give the twisting of clouds that is typically seen
 *   on Venus.
 *
 *
 * PARAMETERS:
 *    Ka, Kd - the usual meaning
 *    offset, scale - control the linear scaling of the cloud value.
 *    twist - controls the twisting of the clouds due to Coriolis forces.
 *    omega - controls the fractal characteristics of the clouds
 *    octaves - the number of octaves of noise to sum for the clouds.
 *
 *
 * HINTS:
 *    The default values for the shader assume that the planet is
 *    represented by a unit sphere.  The texture space and/or parameters
 *    to this shader will need to be altered if the size of your planet
 *    is radically different.
 *
 *
 * AUTHOR: Ken Musgrave.
 *    Conversion to Shading Language and minor modifications by Larry Gritz.
 *
 *
 * REFERENCES:
 *    _Texturing and Modeling: A Procedural Approach_, by David S. Ebert, ed.,
 *    F. Kenton Musgrave, Darwyn Peachey, Ken Perlin, and Steven Worley.
 *   Academic Press, 1998.  ISBN 0-12-228730-4.
 *
 *
 * HISTORY:
 *    ???? - Venus texture developed by F. Ken Musgrave.
 *    Feb 1994 - Conversion to Shading Language by L. Gritz
 *    Apr 1998 - modern SL and antialiasing by lg
 *
 * last modified 4 Apr 1998 by lg
 */



#define TWOPI (2*PI)

#include "noises.h"




surface
venus (float Ka = 1, Kd = 1;
       float offset = 1;
       float scale = 0.6;
       float twist = 0.22;
       float omega = 0.65;
       float octaves = 8;)
{
    point Ptexture;           /* the shade point in texture space */
    vector PtN;               /* normalized version of Ptexture */
    point PP;                 /* Point after rotation by coriolis twist */
    float rsq;                /* Used in calculation of twist */
    float angle;              /* Twist angle */
    float value;              /* Fractal sum is stored here */
    float filtwidth;          /* Filter width for antialiasing */
    
    /* Transform to texture coordinates */
    Ptexture = transform ("shader", P);
    filtwidth = filterwidthp (Ptexture);

    /* Calculate Coriolis twist, yielding point PP */
    PtN = normalize (vector Ptexture);
    rsq = xcomp(PtN)*xcomp(PtN) + ycomp(PtN)*ycomp(PtN);
    angle = twist * TWOPI * rsq;
    PP = rotate (Ptexture, angle, point(0,0,0), point(0,0,1));
    
    /* Compute fBm */
    value = abs (offset + scale * fBm (PP,filtwidth,octaves,2,omega));

    /* Shade like matte, but with color scaled by cloud color */
    Oi = Os;
    Ci = Oi * (value * Cs) * (Ka * ambient() +
			      Kd * diffuse(faceforward(normalize(N),I)));
}
