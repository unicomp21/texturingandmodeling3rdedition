# Makefile for Ken Musgrave's example procedural shaders from Chapter 8 of:
#   _Texturing and Modeling: A Procedural Approach_, by David S. Ebert, ed.,
#   F. Kenton Musgrave, Darwyn Peachey, Ken Perlin, and Steven Worley.
#   Academic Press, 1998.  ISBN 0-12-228730-4.
#


SLO = slc
SHADER = slc
RENDER = rendrib



######################################################################
# Rules for compiling shaders and rendering images
######################################################################

.SUFFIXES: .sl .$(SLO) .tif .rib

.sl.$(SLO):
	$(SHADER) $*.sl

.rib.tif:
	$(RENDER) $*.rib



######################################################################
# The files which we have in the demo distribution
######################################################################

SHADERS= puffyclouds.sl planetclouds.sl terran.sl\
	 starfield.sl cyclone.sl venus.sl flame.sl strata.sl luna.sl\
	 windywave.sl shiny.sl planetatmo.sl platlight.sl

RIBS = clouds.rib planet.rib cyclone.rib venus.rib flame.rib \
	mount.rib planet2.rib

IMAGES = clouds.tif planet.tif cyclone.tif venus.tif flame.tif \
	mount.tif planet2.tif


all: $(IMAGES)



######################################################################
# Individual dependencies
######################################################################

clouds.tif: puffyclouds.$(SLO)

planet.tif: planetclouds.$(SLO) terran.$(SLO) starfield.$(SLO)

cyclone.tif: cyclone.$(SLO)

venus.tif: venus.$(SLO) starfield.$(SLO)

flame.tif: flame.$(SLO)

mount.tif: strata.$(SLO) shiny.$(SLO) luna.$(SLO) windywave.$(SLO)

planet2.tif: planetclouds.$(SLO) terran.$(SLO) starfield.$(SLO) \
		planetatmo.$(SLO) platlight.$(SLO)


######################################################################

ALLFILES =  README BMRT.Announce Makefile* $(SHADERS) $(RIBS) noises.h


tar:
	tar cvf texture.tar $(ALLFILES)

