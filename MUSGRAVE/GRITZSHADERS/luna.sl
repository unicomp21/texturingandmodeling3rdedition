/*
 * luna.sl -- surface shader for the moon
 *
 * DESCRIPTION:
 *    Makes a surface that looks sort of like Earth's moon.  It doesn't really
 *    have craters, so it isn't good for closeups.  But it's pretty good at about
 *    the scale for human naked-eye viewing from earth.
 *
 * AUTHOR:
 *    C language version by F. Kenton Musgrave
 *    Translation to Shading Language by Larry Gritz.
 *
 * REFERENCES:
 *    _Texturing and Modeling: A Procedural Approach_, by David S. Ebert, ed.,
 *    F. Kenton Musgrave, Darwyn Peachey, Ken Perlin, and Steven Worley.
 *   Academic Press, 1998.  ISBN 0-12-228730-4.
 *
 * HISTORY:
 *    ??? - original C language version by Ken Musgrave
 *    Apr 94 - translation to Shading Language by L. Gritz
 *    Apr 1998 - modern SL and antialiasing by lg
 *
 * last modified 4 Apr 1998 by lg
 */


#include "noises.h"

#define TWOPI (2*PI)


surface
luna (float Ka = .5, Kd = 1;
      float lacunarity = 2;
      float octaves = 8;
      float H = .3;
      color highland_color = .7;
      float maria_basecolor = .7, maria_color = .1;
      float arg22 = 1, arg23 = .3;
      float highland_threshold = -0.2;
      float highland_altitude = 0.001, maria_altitude = 0.0004;
      float peak_rad = .0075, inner_rad = .01, rim_rad = .02, outer_rad = .05;
      float peak_ht = 0.005, rim_ht = 0.003;
      float numrays = 8;  /* arg10 */
      float rayfade = 1;  /* arg11 */
      )
{
    float radial_dist;
    point PP, PQ;
    float chaos;
    color Ct;
    float temp1;
    vector vv;
    float uu, ht;
    float lighten;
    normal NN;
    float pd;  /* pole distance */
    float raydist;
    float filtwidth;
    float omega;

    PQ = P;
    PP = transform ("shader", P);
    filtwidth = filterwidthp(PP);
    NN = normalize (N);
    radial_dist = sqrt (xcomp(PP)*xcomp(PP) + ycomp(PP)*ycomp(PP));
    omega = pow (lacunarity, (-.5)-H);

    chaos = fBm (PP, filtwidth, octaves, lacunarity, omega);
    
    Ct = Cs;

    /* Insure that the crater is in one of the maria */
    temp1 = radial_dist * arg22;
    if (temp1 < 1)
	chaos -= arg23 * (1 - smoothstep (0, 1, temp1));

    if (chaos > highland_threshold) {
	PQ += chaos * highland_altitude * NN;
	Ct += highland_color * chaos;
    } else {
	PQ += chaos * maria_altitude * NN;
	Ct *= maria_basecolor + maria_color * chaos;
    }


    /***********************************************************************/
    /* Add crater */
    /* get normalized vector "v" */
    pd = 1-v;
    vv = vector (xcomp(PP)/radial_dist, 0, zcomp(PP)/radial_dist);
    lighten = 0;
    if (pd < peak_rad) {      /* central peak */
	uu = 1 - pd/peak_rad;
/*      lighten = uu*uu; */
	ht = peak_ht * smoothstep (0, 1, uu);
    } else if (pd < inner_rad) {       /* crater floor */
	ht = 0;
    } else if (pd < rim_rad) {           /* inner rim */
	uu = (pd-inner_rad) / (rim_rad - inner_rad);
	lighten = .75*uu;
	ht = rim_ht * smoothstep (0, 1, uu);
    } else if (pd < outer_rad) {        /* outer rim */
	uu = 1 - (pd-rim_rad) / (outer_rad-rim_rad);
	lighten = .75*uu*uu;
	ht = rim_ht * smoothstep (0, 1, uu*uu);
    }
    else ht = 0;
    PQ += ht * NN;
    lighten *= 0.2;
    Ct += color(lighten,lighten,lighten);

    /* Add some noise */
    if (uu > 0) {
	if (pd < peak_rad) {     /* if on central peak */
	    vv = 5*PP + 3 * vv;
	    ht = fBm (vv, filterwidthp(vv), 4, 2, 0.833);
	    PQ += 0.0025 * uu*ht * NN;
        } else {
	    vv = 6*PP + 3 * vv;
	    ht = fBm (vv, filterwidthp(vv), 4, 2, 0.833);
	    if (radial_dist > rim_rad)
		uu *= uu;
	    PQ += 0.0025 * (0.5*uu + 0.5*ht) * NN;
        }
    }

    lighten = 0;
    if (pd >= rim_rad  &&  pd < 0.4) {
	float fw = filterwidth(u);
	lighten = smoothstep (.15, .5, filteredsnoise(62*u,62*fw));
	raydist = 0.2 + 0.2 * filteredsnoise (20 * mod(u+0.022,1), 20*u);
	lighten *= (1 - smoothstep (raydist-.2, raydist, pd));
    }
    lighten = 0.2 * clamp (lighten, 0, 1);
    Ct += color (lighten, lighten, lighten);


    /* Recalc normal since we changed P a whole bunch. */
/*  N = normalize (calculatenormal (PQ)); */

    /* Shade like matte */
    Ci = Ct * (Ka * ambient() + Kd * diffuse(faceforward(normalize(N),I)));
    Oi = Os;  Ci *= Oi;
}

